import java.util.ArrayList;
import java.util.List;

public class GameControl {
	private Player p1;
	private Player p2;
	private List<PlayingField> fieldList;

	public GameControl(Player p1, Player p2) {
		this.p1 = p1;
		this.p2 = p2;
		fieldList = new ArrayList<PlayingField>();
	}

	public void distribute(int fieldPlayed) { // fieldPlayed form 0 to 11
		int seeds = fieldList.get(fieldPlayed).empty();
		Player player = null;
		if (fieldPlayed >= 0 && fieldPlayed <= 5)
			player = p1; // 0-5 for Player 1
		if (fieldPlayed >= 6 && fieldPlayed <= 11)
			player = p2; // 6-11 for Player 2

		fieldPlayed--; // beim n�chsten Feld anfangen

		while (seeds > 0) {
			if (fieldPlayed <= -1) {
				fieldPlayed = 11;
			}
			PlayingField field = fieldList.get(fieldPlayed);
			field.add();

			if (field.getSeeds() == 2 && checkField(player, field)) {
				field.empty();
				player.addScore();
				
			}
			fieldPlayed--;

			seeds--;
		}

	}

	public List<PlayingField> getFieldList() {
		return fieldList;
	}
	public Player getPlayer1() {
		return p1;
	}
	public Player getPlayer2() {
		return p2;
	}

	public void addField() {
		fieldList.add(new PlayingField());
	}

	private boolean checkField(Player player, PlayingField field) {
		int fieldChosen = fieldList.indexOf(field);

		if (player.equals(p1) && fieldChosen >= 6 && fieldChosen <= 11) {
			return true;
		}
		if (player.equals(p2) && fieldChosen >= 0 && fieldChosen <= 5) {
			return true;
		}
		return false;
	}

	public boolean checkGameOver() {
		if (p1.getScore() + p2.getScore() == 46) {
			return true;
		}
		return false;
	}

}
