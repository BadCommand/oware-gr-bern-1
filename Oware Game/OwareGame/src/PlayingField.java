
public class PlayingField{

	private int seeds;
	
	public PlayingField() {
		seeds = 4; // Initialise on 4
	}
	
	public int getSeeds() {
		return seeds;
	}
	public void add() {
		seeds += 1;
	}
	public int empty() {
		int seedsToDistribute = seeds;
		seeds = 0;
		return seedsToDistribute;
	}
}
