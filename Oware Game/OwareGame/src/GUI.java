import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class GUI extends Application {
	private boolean player1 = false;
	private NonPlayerCharacter computer;
	private GameControl control;

	@Override
	public void start(Stage stage) throws Exception {
		final AnchorPane root = new AnchorPane();
		root.setStyle("-fx-background-color : darkgreen");

		final Rectangle overview = new Rectangle(0, 80, 500, 40);
		final Rectangle setupControl = new Rectangle(0, 80, 500, 60);
		final Button controlButton = new Button();
		final RadioButton gameMode = new RadioButton("2 Player Mode: OFF");
		final Rectangle topGameBoard = new Rectangle(0, 0, 500, 40);
		final Rectangle bottomGameBoard = new Rectangle(0, 40, 500, 40);
		final TextField namePlayer1 = new TextField("Name P1");
		final TextField namePlayer2 = new TextField("Computer");
		final Label overviewLabel = new Label();
		int scorePlayer1 = 0;
		int scorePlayer2 = 0;
		Label scoreP1 = new Label("Player 1: \t" + scorePlayer1);
		Label scoreP2 = new Label("Player 2: \t" + scorePlayer2);
		Group top = new Group();
		Group bottom = new Group();
		final Button[] gameField = new Button[12];
		Label score = new Label("Score:");
		Boolean end = false;
		Label turn = new Label("Your turn: " + namePlayer1);
		Label gameBoardP1 = new Label("P1");
		Label gameBoardP2 = new Label("P2");
		Label gameOver = new Label("Game Over");

		// Overview window settings
		overviewLabel.setText("\tOware: \n The Jungle Advanture");
		overviewLabel.setLayoutX(120);
		overviewLabel.setLayoutY(5);
		overviewLabel.setMaxWidth(350);
		overviewLabel.setWrapText(true);
		overviewLabel.setFont(new Font("Arial", 30));
		overviewLabel.setTextFill(Color.TAN);

		// Button: Start game
		controlButton.setText("Start");
		AnchorPane.setBottomAnchor(controlButton, 7.5);
		AnchorPane.setLeftAnchor(controlButton, 10.0);

		// RadioButton: Change game mode
		AnchorPane.setBottomAnchor(gameMode, 12.5);
		AnchorPane.setLeftAnchor(gameMode, 105.0);
		gameMode.setTextFill(Color.WHITE);

		// TextField: Player name
		namePlayer1.setMaxWidth(80);
		namePlayer1.setLayoutX(300);
		namePlayer1.setLayoutY(85);
		namePlayer2.setMaxWidth(80);
		namePlayer2.setLayoutX(410);
		namePlayer2.setLayoutY(85);
		namePlayer2.setDisable(true);

		// EventHandler: Change state depending on RadioButton
		gameMode.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent actionEvent) {
				if (gameMode.isSelected()) {
					namePlayer2.setDisable(false);
					gameMode.setText("2 Player Mode: ON");
					namePlayer2.setText("Name P2");
				} else {
					namePlayer2.setDisable(true);
					namePlayer2.setText("Computer");
					gameMode.setText("2 Player Mode: OFF");
				}
			}
		});

		// Set everything on window
		root.getChildren().addAll(overview, setupControl, controlButton, gameMode, namePlayer1, namePlayer2,
				overviewLabel);

		controlButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent actionEvent) {

				// create Control and add Gamefields
				control = new GameControl(new Player(namePlayer1.getText()), new Player(namePlayer2.getText()));
				for (int i = 0; i < 12; i++) {
					control.addField();
				}

				// Initialise Data
				scoreP1.setText(namePlayer1.getText() + ": \t" + scorePlayer1);
				scoreP2.setText(namePlayer2.getText() + ": \t" + scorePlayer2);
				turn.setText("Your turn: " + namePlayer1.getText());
				score.setText("Score: ");
				namePlayer1.setVisible(false);
				namePlayer2.setVisible(false);

				// create Computer if Computer-Gamemode is selected
				if (!gameMode.isSelected()) {
					computer = new NonPlayerCharacter(control);
				}

				// Change GUI style and remove overview objects
				root.setStyle("-fx-background-color : black");
				root.getChildren().removeAll(overview, setupControl, controlButton, gameMode, overviewLabel, score,
						gameOver);
				namePlayer1.setDisable(true);
				namePlayer2.setDisable(true);
				gameOver.setVisible(false);
				gameOver.setStyle("-fx-background-color: WHITE");
				gameOver.setLayoutX(125);
				gameOver.setLayoutY(10);
				gameOver.setFont(new Font("Arial", 50));

				// Rectangle: Game boards
				topGameBoard.setFill(Color.BROWN);
				topGameBoard.setStroke(Color.BLACK);
				gameBoardP1.setTextFill(Color.BLACK);
				gameBoardP2.setTextFill(Color.BLACK);
				gameBoardP1.setLayoutX(15);
				gameBoardP1.setLayoutY(10);
				gameBoardP2.setLayoutX(15);
				gameBoardP2.setLayoutY(50);
				top.getChildren().addAll(topGameBoard, gameBoardP1);
				bottomGameBoard.setFill(Color.BROWN);
				bottomGameBoard.setStroke(Color.BLACK);
				bottom.getChildren().addAll(bottomGameBoard, gameBoardP2);

				int w = 50;
				int h = 5;
				for (int i = 0; i < 12; i++) {
					// Initialize gameField 0 - 5 = Player 1; 6 - 11 = Player 2
					if (i < 6) {
						gameField[i] = new Button(Integer.toString(control.getFieldList().get(i).getSeeds())); // Integer.toString(control.getFieldList().get(i).getSeeds())
						gameField[i].setLayoutX(w);
						gameField[i].setLayoutY(h);
						gameField[i].setShape(new Circle(15));
						gameField[i].setTextFill(Color.ANTIQUEWHITE);
						gameField[i].setBackground(new Background(new BackgroundFill(Color.BLACK, null, null)));
						w += 80;
						top.getChildren().addAll(gameField[i]);

					} else {
						w -= 80;
						gameField[i] = new Button(Integer.toString(control.getFieldList().get(i).getSeeds())); // Integer.toString(control.getFieldList().get(i).getSeeds())
						gameField[i].setLayoutX(w);
						gameField[i].setLayoutY(h + 40);
						gameField[i].setShape(new Circle(15));
						gameField[i].setTextFill(Color.ANTIQUEWHITE);
						gameField[i].setBackground(new Background(new BackgroundFill(Color.BLACK, null, null)));
						bottom.getChildren().addAll(gameField[i]);
					}
				}

				// Button: Close application
				controlButton.setText("End");
				AnchorPane.setBottomAnchor(controlButton, 7.5);
				AnchorPane.setLeftAnchor(controlButton, 10.0);

				// Label: Score
				AnchorPane.setBottomAnchor(scoreP1, 20.0);
				AnchorPane.setLeftAnchor(scoreP1, 140.0);
				AnchorPane.setBottomAnchor(scoreP2, 5.0);
				AnchorPane.setLeftAnchor(scoreP2, 140.0);
				AnchorPane.setBottomAnchor(score, 10.0);
				AnchorPane.setLeftAnchor(score, 65.0);
				AnchorPane.setBottomAnchor(turn, 10.0);
				AnchorPane.setLeftAnchor(turn, 300.0);
				scoreP1.setTextFill(Color.WHITE);
				scoreP2.setTextFill(Color.WHITE);
				score.setTextFill(Color.WHITE);
				score.setFont(new Font("Arial", 20));
				turn.setTextFill(Color.WHITE);
				turn.setFont(new Font("Arial", 20));
				// score.setUnderline(true);

				update(gameField, scoreP1, scoreP2, end, turn, gameOver);

				// Button Actions

				gameField[0].setOnAction(e -> {
					if (player1) {
						control.distribute(0);
						update(gameField, scoreP1, scoreP2, end, turn, gameOver);
					}

				});
				gameField[1].setOnAction(e -> {
					if (player1) {
						control.distribute(1);
						update(gameField, scoreP1, scoreP2, end, turn, gameOver);
					}
				});
				gameField[2].setOnAction(e -> {
					if (player1) {
						control.distribute(2);
						update(gameField, scoreP1, scoreP2, end, turn, gameOver);
					}
				});
				gameField[3].setOnAction(e -> {
					if (player1) {
						control.distribute(3);
						update(gameField, scoreP1, scoreP2, end, turn, gameOver);
					}
				});
				gameField[4].setOnAction(e -> {
					if (player1) {
						control.distribute(4);
						update(gameField, scoreP1, scoreP2, end, turn, gameOver);
					}
				});
				gameField[5].setOnAction(e -> {
					if (player1) {
						control.distribute(5);
						update(gameField, scoreP1, scoreP2, end, turn, gameOver);
					}
				});

				gameField[6].setOnAction(e -> {
					if (!player1) {
						control.distribute(6);
						update(gameField, scoreP1, scoreP2, end, turn, gameOver);
					}
				});
				gameField[7].setOnAction(e -> {
					if (!player1) {
						control.distribute(7);
						update(gameField, scoreP1, scoreP2, end, turn, gameOver);
					}
				});
				gameField[8].setOnAction(e -> {
					if (!player1) {
						control.distribute(8);
						update(gameField, scoreP1, scoreP2, end, turn, gameOver);
					}
				});
				gameField[9].setOnAction(e -> {
					if (!player1) {
						control.distribute(9);
						update(gameField, scoreP1, scoreP2, end, turn, gameOver);
					}
				});
				gameField[10].setOnAction(e -> {
					if (!player1) {
						control.distribute(10);
						update(gameField, scoreP1, scoreP2, end, turn, gameOver);
					}
				});
				gameField[11].setOnAction(e -> {
					if (!player1) {
						control.distribute(11);
						update(gameField, scoreP1, scoreP2, end, turn, gameOver);
					}
				});

				// Set everything on window
				root.getChildren().addAll(top, bottom, controlButton, scoreP1, scoreP2, score, turn, gameOver);

				// Close application
				controlButton.setOnAction(e -> Platform.exit());

			}
		});

		// Let the show begin
		stage.setTitle("Oware Game Version 1.0");
		stage.setScene(new Scene(root, 500, 120));
		stage.setResizable(false);
		stage.show();
	}

	public void update(Button[] gameField, Label scoreP1, Label scoreP2, Boolean end, Label turn,
			Label gameOver) {
		// update Seeds
		for (int i = 0; i < 12; i++) {
			gameField[i].setText("" + control.getFieldList().get(i).getSeeds());
		}
		// update Score
		scoreP1.setText(control.getPlayer1().getName() + ": \t" + control.getPlayer1().getScore());
		scoreP2.setText(control.getPlayer2().getName() + ": \t" + control.getPlayer2().getScore());

		// update Player playing
		if (player1) {

			// check if all fields 0
			int sum = 0;
			for (int i = 6; i < 12; i++) {
				sum += control.getFieldList().get(i).getSeeds();
			}
			// Disable Buttons with 0 and Enable others
			for (int i = 0; i < 12; i++) {
				if (control.getFieldList().get(i).getSeeds() == 0) {
					gameField[i].setDisable(true);
				} else
					gameField[i].setDisable(false);
			}
			
			if (sum != 0) {

				for (int i = 0; i < 6; i++) {
					gameField[i].setDisable(true);
				}

				player1 = false;

				turn.setText("Your turn: " + control.getPlayer2().getName());

				// computer turn
				if (computer != null) {
					int test = computer.computerTurn();
					while (control.getFieldList().get(test).getSeeds() == 0) {
						test = computer.computerTurn();
					}
					// trigger Event
					
					gameField[test].fire();
					gameField[test].setDisable(false);
					

				}
			}
		} else {
			

			// Disable Buttons with 0
			for (int i = 0; i < 12; i++) {
				if (control.getFieldList().get(i).getSeeds() == 0) {
					gameField[i].setDisable(true);
				} else
					gameField[i].setDisable(false);
			}

			int sum = 0;
			for (int i = 0; i < 6; i++) {
				sum += control.getFieldList().get(i).getSeeds();
			}
			if (sum != 0) {

				for (int i = 6; i < 12; i++) {
					gameField[i].setDisable(true);
				}

				player1 = true;

				turn.setText("Your turn: " + control.getPlayer1().getName());
			}
			if (computer != null && sum==0) {
				int test = computer.computerTurn();
				while (control.getFieldList().get(test).getSeeds() == 0) {
					test = computer.computerTurn();
				}
				// trigger Event
				
				gameField[test].fire();
			}
		}

		// End Game Window
		end = control.checkGameOver();
		if (end == true)
			gameOver.setVisible(true);

	}

	public static void main(String[] args) {
		launch(args);
	}

}
