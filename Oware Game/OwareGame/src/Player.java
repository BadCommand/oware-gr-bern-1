
public class Player {
	private int score;
	private String name;

	public Player(String name) {
		score = 0;
		this.name = name;
	}

	public int getScore() {
		return score;
	}

	public String getName() {
		return name;
	}

	public void addScore() {
		score += 2;
	}

}
