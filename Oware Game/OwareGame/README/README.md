---------------------------------------------------------
%   Name:       Oware Game - The Jungle Advanture       %
%   Version:    1.0                                     %
%   Copyright:  Falvella, Antonini & Reinbold           %
%   Date:       01/10/2020                              %
%                                                       %
%   Build in:   Eclipse IDE for Java Developers         %
%   Version:    2019-09 R (4.13.0)                      %
%   Build id:   20190917-1200                           %
%   OS:         Mac OS X, v.10.15.2, x86_64 / cocoa     %
%   Java Vers.: 11.0.4                                  %  
%                                                       %
---------------------------------------------------------

# Introduction

This is the Oware Game (Version 1.0) Readme written by Falvella, Antonini and Reinbold. Oware is a two-player board game played in
different parts of the world under different names (Awari, Ouril, etc.) and with slightly different rules. In this project, we 
consider a simplified version of the Oware game.
Now you will get important instructions for setting up built environment. For software structure please have a look at the UML diagram:

# UML Diagram

![](class_diagram.jpg)

# Import
1. Import workspace from GIT repository
2. Make sure you have added: JavaFX13 library to your project (instructions see https://openjfx.io/openjfx-docs/ and follow steps until instruction #4)
2. Press "Run button" in IDE of your choice

# Responsibilities

1. GUI:  Game handling
2. GameControl:  Handling of player turns, game mode and play commands
3. NonCharacterPlayer:   Computer player algorithm
4. PlayingField:   Point distribution handling
5. Player:   Name and points handling

# Rules

1. The game board consists of two rows of six pits. Each row belongs to a player. 
2. The game starts with 4 seeds in each of the 12 pits (48 seeds in total).
3. The two players play in turns. At each turn, a player chooses one of his 
   pits (which must contain at least one seed) and distributes the seeds 
   counter-clockwise around the board.
4. The goal of the game is to take seeds on the opponent's pit row. Seeds can 
   be taken when a single seed is in a pit by placing a second seed in the pit. 
   If this happens, both seeds are removed from the pit.
5. The game ends if only 2 seeds remain in the game. Therefore, the possible 
   final scores are 0:46, 2:44 to 46:0. Note that there is always a winner.

# Remarks
* As long as at least 4 seeds remain in the game, there exists a strategy to avoid an endless loop.
* Sometimes, more than one seed can be taken simultaneously in different pits of the opponent.
* If all pits of a player are empty, the other player can play multiple times, until the first player gets a seed.
* If a large number of seeds are in the same pit, distributing the seeds may go over more than one row.

**Good luck and enjoy playing!**

 
